/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include "../../libpfrng.h"

int main(int argc, char** argv)
{
	int opts;
	unsigned int fast = 0;
	struct option longopts[] =
	{
		{"fast", no_argument, NULL, 'f'},
		{0, 0, 0, 0}
	};
	while ((opts = getopt_long(argc, argv, "f", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'f':
				fast = 1;
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}


	pfrng_init();

	FILE* fout = fopen("/dev/stdout", "w");
	uint64_t n;
	while (1)
	{
		if (fast)
			n = pfrng_get_u64_fast();
		else
			n = pfrng_get_u64();
		fwrite(&n, sizeof(n), 1, fout);
		fflush(fout);
	}
	fclose(fout);

	pfrng_done();

	exit(EX_OK);
}

