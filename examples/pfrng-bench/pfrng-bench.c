/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#if defined (_OPENMP)
#include <omp.h>
#endif
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/time.h>
#include <sysexits.h>
#include <time.h>

#include "../../libpfrng.h"

unsigned long long calls = 0, ticks = 0;
double sum = 0;
pthread_mutex_t calls_lock = PTHREAD_MUTEX_INITIALIZER;

static void timer_tick(union sigval _arg)
{
	(void)_arg;

	unsigned long long res;
	pthread_mutex_lock(&calls_lock);
	res = calls;
	calls = 0;
	pthread_mutex_unlock(&calls_lock);

	double speed = (double)(res * SIZEOF_UINT64) / 1024.0;
	sum += speed;
	ticks++;
	double avg_speed = sum / (double)ticks;

	printf("Current speed: %1.3lf KiB/s\tAverage speed: %1.3lf KiB/s\n", speed, avg_speed);
}

int main(int argc, char** argv)
{
	timer_t timer_info;
	struct sigevent timer_event;
	timer_event.sigev_notify = SIGEV_THREAD;
	timer_event.sigev_notify_function = timer_tick;
	timer_event.sigev_notify_attributes = NULL;
	struct itimerspec timer_time;
	timer_time.it_interval.tv_sec = 1;
	timer_time.it_interval.tv_nsec = 0;
	timer_time.it_value.tv_sec = 1;
	timer_time.it_value.tv_nsec = 0;

	int opts;
	unsigned int fast = 0;
	struct option longopts[] =
	{
		{"fast", no_argument, NULL, 'f'},
		{0, 0, 0, 0}
	};
	while ((opts = getopt_long(argc, argv, "f", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'f':
				fast = 1;
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}

	pfrng_init();

        //detect CPUs count
#if defined (_OPENMP)
	int cpus = omp_get_max_threads();
#else
	int cpus = 1;
#endif
	printf("Using %d thread(s)\n", cpus);

	volatile uint64_t n __attribute__ ((unused));

	timer_create(CLOCK_REALTIME, &timer_event, &timer_info);
	timer_settime(timer_info, 0, &timer_time, NULL);
	while (1)
	{
		if (fast)
			n = pfrng_get_u64_fast();
		else
			n = pfrng_get_u64();

		pthread_mutex_lock(&calls_lock);
		calls++;
		pthread_mutex_unlock(&calls_lock);
	}

	pfrng_done();

	exit(EX_OK);
}

