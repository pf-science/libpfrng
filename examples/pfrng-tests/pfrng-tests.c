/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include "../../libpfrng.h"

#define PASSES		1000000
#define BUFFER1_SIZE	113
#define BUFFER2_SIZE	27

int main(int argc, char** argv)
{
	int opts;
	unsigned int fast = 0;
	struct option longopts[] =
	{
		{"fast", no_argument, NULL, 'f'},
		{0, 0, 0, 0}
	};
	while ((opts = getopt_long(argc, argv, "f", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'f':
				fast = 1;
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}


	pfrng_init();

	double sum;
	FILE *fout;

	printf("Performing normalization test for uniform distribution...\n");
	sum = 0;
	fout = fopen("out.uniform", "w");
	for (uint64_t i = 0; i < PASSES; i++)
	{
		double rn;
		if (fast)
			rn = pfrng_get_lf_fast();
		else
			rn = pfrng_get_lf();
		sum += rn;
		fprintf(fout, "%1.16lf\n", rn);
		fflush(fout);
	}
	fclose(fout);
	printf("%1.16lf\n", sum / PASSES);

	printf("Performing normalization test for normal distribution...\n");
	sum = 0;
	fout = fopen("out.normal", "w");
	for (uint64_t i = 0; i < PASSES; i++)
	{
		double rn;
		if (fast)
			rn = pfrng_get_gauss_fast();
		else
			rn = pfrng_get_gauss();
		sum += rn;
		fprintf(fout, "%1.16lf\n", rn);
		fflush(fout);
	}
	fclose(fout);
	printf("%1.16lf\n", sum / PASSES);

	printf("Filling %u bytes buffer...\n", BUFFER1_SIZE);
	unsigned char *buffer1 = malloc(BUFFER1_SIZE * sizeof(unsigned char));
	printf("Buffer 1 address: %p\n", buffer1);
	uint64_t buffer1_filled;
	if (fast)
		buffer1_filled = pfrng_fill_buffer_u_fast(buffer1, BUFFER1_SIZE);
	else
		buffer1_filled = pfrng_fill_buffer_u(buffer1, BUFFER1_SIZE);
	printf("%ju bytes filled:\n", buffer1_filled);
	for (uint8_t i = 0; i < BUFFER1_SIZE; i++)
	{
		if (i % 40 == 0 && i != 0)
			printf("\n");
		printf("%02x ", buffer1[i]);
	}
	printf("\n");

	printf("Filling %u bytes buffer...\n", BUFFER2_SIZE);
	unsigned char *buffer2 = malloc(BUFFER2_SIZE * sizeof(unsigned char));
	printf("Buffer 2 address: %p\n", buffer2);
	uint64_t buffer2_filled;
	if (fast)
		buffer2_filled = pfrng_fill_buffer_u_fast(buffer2, BUFFER2_SIZE);
	else
		buffer2_filled = pfrng_fill_buffer_u(buffer2, BUFFER2_SIZE);
	printf("%ju bytes filled:\n", buffer2_filled);
	for (uint8_t i = 0; i < BUFFER2_SIZE; i++)
	{
		if (i % 40 == 0 && i != 0)
			printf("\n");
		printf("%02x ", buffer2[i]);
	}
	printf("\n");

	printf("Estimating pi value...\n");
	uint64_t pi_in = 0;
	double p = 0;
	for (uint64_t i = 0; i < PASSES; i++)
	{
		double X = 0, Y = 0, D = 0;
		if (fast)
		{
			X = pfrng_get_lf_fast();
			Y = pfrng_get_lf_fast();
		} else
		{
			X = pfrng_get_lf();
			Y = pfrng_get_lf();
		}

		D = pow(X, 2) + pow(Y, 2);
		if (D <= 1)
			pi_in++;
	}
	p = (double)pi_in / (double)PASSES;
	printf("pi≈%lf\n", 4.0 * p);

	printf("Building frequency table...\n");
	fout = fopen("out.freq", "w");
	pfrng_stats_t stats = pfrng_get_stats();
	for (uint64_t i = 0; i < UINT64_SIZE; i++)
	{
		fprintf(fout, "%ju\t%ju\n", i, stats.freq[i]);
		fflush(fout);
	}
	fclose(fout);
	
	printf("Bits misses: %ju\n", stats.bits_misses);

	pfrng_done();

	exit(EX_OK);
}

