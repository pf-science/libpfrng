/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include "../../libpfrng.h"

#define RNDFILESIZE	(1 * 1024 * 1024)
#define CHUNK_SIZE	(CHAR_BIT * sizeof(uint8_t))

int main(int argc, char** argv)
{
	int opts;
	unsigned int fast = 0;
	struct option longopts[] =
	{
		{"fast", no_argument, NULL, 'f'},
		{0, 0, 0, 0}
	};
	while ((opts = getopt_long(argc, argv, "f", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'f':
				fast = 1;
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}


	pfrng_init();

	printf("Writing random file, %d MiB...\n", RNDFILESIZE / (1024 * 1024));
	FILE* fout = fopen("out.rnd", "w");
	for (uint64_t i = 0; i < RNDFILESIZE; i += CHUNK_SIZE)
	{
		uint64_t pool;
		if (fast)
			pool = pfrng_get_u64_fast();
		else
			pool = pfrng_get_u64();

		for (uint8_t i = 0; i < CHUNK_SIZE; i++)
		{
			uint8_t current_chunk = (uint8_t)pool;
			pool >>= CHUNK_SIZE;
			fprintf(fout, "%u", current_chunk % 10);
			fflush(fout);
		}
	}
	fclose(fout);

	pfrng_done();

	exit(EX_OK);
}

