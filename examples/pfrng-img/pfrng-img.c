/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>

#include "../../libpfrng.h"

#define IMG_WIDTH	480
#define IMG_HEIGHT	640
#define DATA_SIZE	(IMG_WIDTH * IMG_HEIGHT)

typedef unsigned char uchar;

int main(int argc, char** argv)
{
	int opts;
	unsigned int fast = 0;
	char* output = NULL;
	struct option longopts[] =
	{
		{"fast",	no_argument,		NULL, 'f'},
		{"output",	required_argument,	NULL, 'o'},
		{0, 0, 0, 0}
	};
	while ((opts = getopt_long(argc, argv, "fo", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'f':
				fast = 1;
				break;
			case 'o':
				output = malloc(strlen(optarg) * CHAR_BIT);
				if (output == NULL)
					abort();
				strcpy(output, optarg);
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}

	if (output == NULL)
		output = "image.pgm";

	pfrng_init();

	unsigned char *buff = (uchar *)malloc(DATA_SIZE * sizeof(uchar));

	for (uint64_t i = 0; i < DATA_SIZE; i++)
	{
		if (fast)
			buff[i] = pfrng_get_u64_fast() % 256;
		else
			buff[i] = pfrng_get_u64() % 256;
	}

	FILE *image = fopen(output, "wb");
	if (image == NULL)
	{
		fprintf(stderr, "Can't open output file %s!\n", argv[1]);
		exit(1);
	}
	fprintf(image, "P5\n%u %u 255\n", IMG_WIDTH, IMG_HEIGHT);
	fwrite(buff, 1, DATA_SIZE * sizeof(unsigned char), image);
	fclose(image);
	free(buff);

	pfrng_done();

	return 0;
}

