/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#define DEV_RANDOM		"/dev/random"
#define POOL_SIZE		"/proc/sys/kernel/random/poolsize"
#define WATERMARK		"/proc/sys/kernel/random/write_wakeup_threshold"

#define PFRNG_FEEDER_SOCKET	"ipc:///run/pfrng-feeder"

#define PFMSG_ECHO_REQUEST	1
#define PFMSG_EXIT		2
#define PFMSG_ECHO_REPLY	3
#define PFMSG_BYTES_FED_REQUEST	4
#define PFMSG_BYTES_FED_REPLY	5

typedef struct pfrng_msg
{
	signed long long key, value;
} pfrng_msg_t;

