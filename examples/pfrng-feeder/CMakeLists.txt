add_executable(pfrng-feeder pfrng-feeder.c panic.c)
add_executable(pfrng-feeder-ctl pfrng-feeder-ctl.c panic.c)
target_link_libraries(pfrng-feeder pfrng gomp zmq)
target_link_libraries(pfrng-feeder-ctl pfrng gomp zmq)
