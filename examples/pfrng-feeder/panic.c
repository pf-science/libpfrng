/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <execinfo.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sysexits.h>
#include <syslog.h>

#include "panic.h"

static void pfpanic_show_stacktrace(unsigned short _syslog)
{
	void* buffer[PF_PANIC_BACKTRACE_SIZE];
	size_t buffer_size;
	char** symbols;

	buffer_size = backtrace(buffer, PF_PANIC_BACKTRACE_SIZE);
	symbols = backtrace_symbols(buffer, buffer_size);

	for (size_t i = 0; i < buffer_size; i++)
		if (_syslog)
			syslog(LOG_ERR, "%s\n", symbols[i]);
		else
			printf("%s\n", symbols[i]);

	free(symbols);
}

static void pfpanic(const char* _msg, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno, int _status)
{
	if (_syslog)
	{
		if (_verbose)
			syslog(LOG_ERR, "%s: file=%s, function=%s, line=%d (%s)\n", _msg, _file_name, _function_name, _line_number, strerror(_errno));
	} else
		fprintf(stderr, "%s: file=%s, function=%s, line=%d (%s)\n", _msg, _file_name, _function_name, _line_number, strerror(_errno));
	pfpanic_show_stacktrace(_syslog);
	if (_syslog)
		abort();
	else
		exit(_status);
}

void pfpanic_non_root(void)
{
	fprintf(stderr, "You have to be root in order to use this tool!\n");
	exit(EX_NOPERM);
}

void __pfpanic_open(const char* _failed_file, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	const char* msg = "Unable to open file ";
	char* new_msg = malloc(strlen(msg) + strlen(_failed_file) + 1);
	if (new_msg == NULL)
		pfpanic_malloc();
	strcpy(new_msg, msg);
	strcat(new_msg, _failed_file);
	pfpanic(new_msg,
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSFILE);
	free(new_msg);
}

void __pfpanic_read(const char* _failed_file, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	const char* msg = "Unable to read file ";
	char* new_msg = malloc(strlen(msg) + strlen(_failed_file) + 1);
	if (new_msg == NULL)
		pfpanic_malloc();
	strcpy(new_msg, msg);
	strcat(new_msg, _failed_file);
	pfpanic(new_msg,
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSFILE);
	free(new_msg);
}

void __pfpanic_close(const char* _failed_file, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	const char* msg = "Unable to close file ";
	char* new_msg = malloc(strlen(msg) + strlen(_failed_file) + 1);
	if (new_msg == NULL)
		pfpanic_malloc();
	strcpy(new_msg, msg);
	strcat(new_msg, _failed_file);
	pfpanic(new_msg,
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSFILE);
	free(new_msg);
}

void __pfpanic_ioctl(const char* _ioctl, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	const char* msg = "Unable to execute IOCTL ";
	char* new_msg = malloc(strlen(msg) + strlen(_ioctl) + 1);
	if (new_msg == NULL)
		pfpanic_malloc();
	strcpy(new_msg, msg);
	strcat(new_msg, _ioctl);
	pfpanic(new_msg,
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
	free(new_msg);
}

void __pfpanic_zero(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unexpected zero occurred",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_signal(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to send signal",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_sigmask(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to manipulate sigmask",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_pselect(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Error occurred while doing pselect()",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_malloc(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Error occurred on malloc() call",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_daemon(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to become daemon",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_pthread_create(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to create thread",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_pthread_join(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to join thread",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_zmq_context_create(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to create ZeroMQ context",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_socket_create(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to create ZeroMQ socket",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_socket_bind(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to bind to ZeroMQ socket",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_socket_connect(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to connect to ZeroMQ socket",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_send(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to send to ZeroMQ socket",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_recv(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to receive from ZeroMQ socket",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_close(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to close ZeroMQ socket",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_zmq_ctx_term(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to destroy ZeroMQ context",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_SOFTWARE);
}

void __pfpanic_clock_gettime(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to call clock_gettime()",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_mutex_lock(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to lock mutex",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_mutex_unlock(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to unlock mutex",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

void __pfpanic_mutex_init(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to init mutex",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}
void __pfpanic_mutex_destroy(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno)
{
	pfpanic("Unable to destroy mutex",
			_syslog,
			_verbose,
			_function_name,
			_file_name,
			_line_number,
			_errno,
			EX_OSERR);
}

