/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <fcntl.h>
#include <getopt.h>
#include <limits.h>
#include <linux/random.h>
#if defined (_OPENMP)
#include <omp.h>
#endif
#include <pthread.h>
#include <unistd.h>
#include <signal.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/ioctl.h>
#include <sysexits.h>
#include <syslog.h>
#include <zmq.h>

#include "../../libpfrng.h"
#include "panic.h"

#include "pfrng-feeder.h"

static int fd;
static uint64_t pool_size = 0;
static unsigned short verbose = 0, should_terminate = 0;
static unsigned long long total_bytes_fed = 0;
static pthread_mutex_t terminate_lock, total_bytes_fed_lock;
static pthread_t mq_worker_id, feeder_worker_id;

static void* mq_worker(void* _data)
{
	(void)_data;

	unsigned short should_exit = 0;
	void *mq_context = NULL, *mq_socket = NULL;

	mq_context = zmq_ctx_new();
	if (mq_context == NULL)
		pfpanic_zmq_context_create_syslog(verbose);

	mq_socket = zmq_socket(mq_context, ZMQ_REP);
	if (mq_socket == NULL)
		pfpanic_zmq_socket_create_syslog(verbose);

	int mq_bind = zmq_bind(mq_socket, PFRNG_FEEDER_SOCKET);
	if (mq_bind != 0)
		pfpanic_zmq_socket_bind_syslog(verbose);

	pfrng_msg_t mq_msg, mq_rsp;
	while (1)
	{
		if (zmq_recv(mq_socket, &mq_msg, sizeof(mq_msg), 0) == -1)
			pfpanic_zmq_recv_syslog(verbose);

		switch (mq_msg.key)
		{
			case PFMSG_ECHO_REQUEST:
				if (verbose)
					syslog(LOG_INFO, "Got PING command\n");

				mq_rsp.key = PFMSG_ECHO_REPLY;
				if (zmq_send(mq_socket, &mq_rsp, sizeof(mq_rsp), 0) == -1)
					pfpanic_zmq_send_syslog(verbose);
				break;
			case PFMSG_BYTES_FED_REQUEST:
				mq_rsp.key = PFMSG_BYTES_FED_REPLY;
				if (pthread_mutex_lock(&total_bytes_fed_lock) != 0)
					pfpanic_mutex_lock();
				unsigned long long local_bytes_fed = total_bytes_fed;
				if (pthread_mutex_unlock(&total_bytes_fed_lock) != 0)
					pfpanic_mutex_unlock();
				mq_rsp.value = local_bytes_fed;
				if (zmq_send(mq_socket, &mq_rsp, sizeof(mq_rsp), 0) == -1)
					pfpanic_zmq_send_syslog(verbose);
				break;
			case PFMSG_EXIT:
				if (verbose)
					syslog(LOG_INFO, "Got EXIT command, attempting to shutdown gracefully...\n");

				should_exit = 1;
				break;
			default:
				break;
		}

		if (should_exit)
		{
			if (zmq_close(mq_socket) == -1)
				pfpanic_zmq_close_syslog(verbose);
			if (zmq_ctx_term(mq_context) == -1)
				pfpanic_zmq_ctx_term_syslog(verbose);

			if (pthread_mutex_lock(&terminate_lock) != 0)
				pfpanic_mutex_lock_syslog(verbose);
			should_terminate = 1;
			if (pthread_mutex_unlock(&terminate_lock) != 0)
				pfpanic_mutex_unlock_syslog(verbose);
			if (pthread_kill(feeder_worker_id, SIGUSR1) != 0)
				pfpanic_signal_syslog(verbose);
			break;
		}
	}

	return NULL;
}

static void sigusr1_handler(int _signo)
{
	(void)_signo;

	return;
}

static void* feeder_worker(void* _data)
{
	(void)_data;

	struct sigaction s;
	sigset_t newset, oldset;

	s.sa_handler = sigusr1_handler;
	if (sigemptyset(&s.sa_mask) != 0)
		pfpanic_sigmask_syslog(verbose);
	s.sa_flags = 0;
	if (sigaction(SIGUSR1, &s, NULL) != 0)
		pfpanic_sigmask_syslog(verbose);

	if (sigemptyset(&newset) != 0)
		pfpanic_sigmask_syslog(verbose);
	if (sigaddset(&newset, SIGUSR1) != 0)
		pfpanic_sigmask_syslog(verbose);
	if (pthread_sigmask(SIG_BLOCK, &newset, &oldset) != 0)
		pfpanic_sigmask_syslog(verbose);

	while (1)
	{
		fd_set wfds;

		FD_ZERO(&wfds);
		FD_SET(fd, &wfds);

		if (pselect(fd + 1, NULL, &wfds, NULL, NULL, &oldset) == -1)
		{
			if (errno == EINTR)
			{
				unsigned short really_terminate;
				if (pthread_mutex_lock(&terminate_lock) != 0)
					pfpanic_mutex_lock_syslog(verbose);
				really_terminate = should_terminate;
				if (pthread_mutex_unlock(&terminate_lock) != 0)
					pfpanic_mutex_unlock_syslog(verbose);
				if (really_terminate)
					break;
			} else
				pfpanic_pselect_syslog(verbose);
		}

		uint64_t entropy_avail = 0;
		if (ioctl(fd, RNDGETENTCNT, &entropy_avail) == -1)
			pfpanic_ioctl_syslog("RNDGETENTCNT", verbose);
		else
			if (verbose)
				syslog(LOG_INFO, "Current entropy count: %ju\n", entropy_avail);

		uint64_t bits_to_feed = pool_size - entropy_avail;

		if (verbose)
			syslog(LOG_INFO, "Need to feed %ju bits to %s... ", bits_to_feed, DEV_RANDOM);

		uint64_t bytes_to_feed = bits_to_feed / CHAR_BIT;
		unsigned char *data = (unsigned char *)malloc(bytes_to_feed);
		if (data == NULL)
			pfpanic_malloc_syslog(verbose);
		uint64_t bytes_fed = pfrng_fill_buffer_u(data, bytes_to_feed);
		uint64_t bits_fed = bytes_fed * CHAR_BIT;

		struct rand_pool_info *output = (struct rand_pool_info *)malloc(sizeof(struct rand_pool_info) + bytes_fed);
		if (output == NULL)
			pfpanic_malloc_syslog(verbose);

		output->entropy_count = bits_fed;
		output->buf_size = bytes_fed;
		memcpy(&(output->buf[0]), data, bytes_fed);
		memset(data, 0, bytes_fed);

		errno = 0;
		if (ioctl(fd, RNDADDENTROPY, output) == -1)
			pfpanic_ioctl_syslog("RNDADDENTROPY", verbose);
		else
		{
			if (verbose)
				syslog(LOG_INFO, "%ju bits fed\n", bits_fed);
			if (pthread_mutex_lock(&total_bytes_fed_lock) != 0)
				pfpanic_mutex_lock();
			total_bytes_fed += bytes_fed;
			if (pthread_mutex_unlock(&total_bytes_fed_lock) != 0)
				pfpanic_mutex_unlock();
		}

		free(output);
		free(data);
	}

	return NULL;
}

int main(int argc, char** argv)
{
	if (getuid() != 0)
		pfpanic_non_root();

	uint8_t daemonize = 0;
	int opts = 0;

	struct option longopts[] =
	{
		{"daemonize",	0, NULL, 'd'},
		{"verbose",	0, NULL, 'v'},
		{"help",	0, NULL, 'h'},
		{0, 0, 0, 0}
	};

	// parse cmdline options
	while (-1 != (opts = getopt_long(argc, argv, "dvh", longopts, NULL)))
	{
		switch (opts)
		{
			case 'd':
				daemonize = 1;
				break;
			case 'v':
				verbose = 1;
				break;
			case 'h':
				printf("pfrng-feeder — small tool to feed /dev/random with high quality random data\n");
				printf("Version %d.%d.%d, API version %d\n", LIBPFRNG_MAJOR, LIBPFRNG_MINOR, LIBPFRNG_RELEASE, LIBPFRNG_API);
				printf("© Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>, 2013–2014\n");
				printf("Distributed under terms and conditions of LGPLv3. See COPYING file for details.\n");
				printf("\n");
				printf("Usage:\n");
				printf("\tpfrng-feeder [--verbose|-v]\n");
				printf("Switches:\n");
				printf("\t--daemonize,\t\t-d\t— run as daemon\n");
				printf("\t--help,\t\t-h\t— show this help\n");
				printf("\t--verbose,\t-v\t— log events to syslog\n");
				exit(EX_OK);
				break;
			default:
				syslog(LOG_ERR, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}
	}

	if (daemonize)
		if (daemon(0, 1) != 0)
			pfpanic_daemon();

	pfrng_init();

        //detect CPUs count
#if defined (_OPENMP)
	int cpus = omp_get_max_threads();
#else
	int cpus = 1;
#endif
	if (verbose)
		syslog(LOG_INFO, "Using %d thread(s)\n", cpus);

	fd = open(DEV_RANDOM, O_WRONLY);
	if (fd == -1)
		pfpanic_open_syslog(DEV_RANDOM, verbose);
	
	FILE *f_pool_size = fopen(POOL_SIZE, "r");
	if (f_pool_size == NULL)
		pfpanic_open_syslog(POOL_SIZE, verbose);
	if (fscanf(f_pool_size, "%ju", &pool_size) != 1)
		pfpanic_read_syslog(POOL_SIZE, verbose);
	if (fclose(f_pool_size) != 0)
		pfpanic_close_syslog(POOL_SIZE, verbose);

	if (pool_size == 0)
		pfpanic_zero_syslog(verbose);

	FILE *f_watermark = fopen(WATERMARK, "w");
	if (f_watermark == NULL)
		pfpanic_open_syslog(WATERMARK, verbose);
	fprintf(f_watermark, "%ju\n", pool_size / 2);
	if (fclose(f_watermark) != 0)
		pfpanic_close_syslog(WATERMARK, verbose);

	if (pthread_mutex_init(&terminate_lock, NULL) != 0 ||
			pthread_mutex_init(&total_bytes_fed_lock, NULL) != 0)
		pfpanic_mutex_init_syslog(verbose);

	if (pthread_create(&mq_worker_id, NULL, mq_worker, 0) != 0 ||
			pthread_create(&feeder_worker_id, NULL, feeder_worker, 0) != 0)
		pfpanic_pthread_create_syslog(verbose);

	if (pthread_join(mq_worker_id, NULL) != 0 ||
			pthread_join(feeder_worker_id, NULL) != 0)
		pfpanic_pthread_join_syslog(verbose);

	if (pthread_mutex_destroy(&terminate_lock) != 0 ||
			pthread_mutex_destroy(&total_bytes_fed_lock) != 0)
		pfpanic_mutex_destroy_syslog(verbose);

	if (close(fd) != 0)
		pfpanic_close_syslog(DEV_RANDOM, verbose);

	pfrng_done();

	if (verbose)
		syslog(LOG_INFO, "Shutdown sequence completed.\n");

	exit(EX_OK);
}

