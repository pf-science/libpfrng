/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>
#include <getopt.h>
#include <pthread.h>
#include <unistd.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>
#include <time.h>
#include <zmq.h>

#include "../../libpfrng.h"
#include "panic.h"

#include "pfrng-feeder.h"

int main(int argc, char** argv)
{
	if (getuid() != 0)
		pfpanic_non_root();

	int opts = 0;

	struct option longopts[] =
	{
		{"ping",	0, NULL, 'p'},
		{"stats",	0, NULL, 's'},
		{"exit",	0, NULL, 'e'},
		{"help",	0, NULL, 'h'},
		{0, 0, 0, 0}
	};

	void *mq_context = NULL, *mq_socket = NULL;
	mq_context = zmq_ctx_new();
	if (mq_context == NULL)
		pfpanic_zmq_context_create();
	mq_socket = zmq_socket(mq_context, ZMQ_REQ);
	if (mq_socket == NULL)
		pfpanic_zmq_socket_create();
	int mq_connect = zmq_connect(mq_socket, PFRNG_FEEDER_SOCKET);
	if (mq_connect != 0)
		pfpanic_zmq_socket_connect();

	pfrng_msg_t mq_msg, mq_rsp;

	// parse cmdline options
	while (-1 != (opts = getopt_long(argc, argv, "pseh", longopts, NULL)))
	{
		switch (opts)
		{
			case 'p':
				mq_msg.key = PFMSG_ECHO_REQUEST;
				struct timespec time_start, time_end;
				if (clock_gettime(CLOCK_REALTIME, &time_start) == -1)
					pfpanic_clock_gettime();
				if (zmq_send(mq_socket, &mq_msg, sizeof(mq_msg), 0) == -1)
					pfpanic_zmq_send();
				if (zmq_recv(mq_socket, &mq_rsp, sizeof(mq_rsp), 0) == -1)
					pfpanic_zmq_recv();
				if (clock_gettime(CLOCK_REALTIME, &time_end) == -1)
					pfpanic_clock_gettime();
				double ms = (time_end.tv_sec * 1000000000ULL + time_end.tv_nsec -
					time_start.tv_sec * 1000000000ULL - time_start.tv_nsec) / 1000000.0;
				if (mq_rsp.key == PFMSG_ECHO_REPLY)
					printf("Got response from daemon in %1.3lf ms!\n", ms);
				else
					fprintf(stderr, "Got unknown response from daemon!\n");
				break;
			case 's':
				mq_msg.key = PFMSG_BYTES_FED_REQUEST;
				if (zmq_send(mq_socket, &mq_msg, sizeof(mq_msg), 0) == -1)
					pfpanic_zmq_send();
				if (zmq_recv(mq_socket, &mq_rsp, sizeof(mq_rsp), 0) == -1)
					pfpanic_zmq_recv();
				if (mq_rsp.key == PFMSG_BYTES_FED_REPLY)
				{
					printf("Total bytes fed by daemon: %lld\n", mq_rsp.value);
				} else
					fprintf(stderr, "Got unknown response from daemon!\n");
				break;
			case 'e':
				mq_msg.key = PFMSG_EXIT;
				if (zmq_send(mq_socket, &mq_msg, sizeof(mq_msg), 0) == -1)
					pfpanic_zmq_send();
				break;
			case 'h':
				printf("pfrng-feeder-ctl — small tool to control /dev/random feeder\n");
				printf("Version %d.%d.%d, API version %d\n", LIBPFRNG_MAJOR, LIBPFRNG_MINOR, LIBPFRNG_RELEASE, LIBPFRNG_API);
				printf("© Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>, 2013–2014\n");
				printf("Distributed under terms and conditions of LGPLv3. See COPYING file for details.\n");
				printf("\n");
				printf("Usage:\n");
				printf("\tpfrng-feeder-ctl [--ping|--stats|--exit]\n");
				printf("Switches:\n");
				printf("\t--ping,\t\t-p\t— ping daemon\n");
				printf("\t--stats,\t\t-s\t— show various daemon statistics\n");
				printf("\t--exit,\t\t-e\t— tell daemon to exit\n");
				printf("\t--help,\t\t-h\t— show this help\n");
				exit(EX_OK);
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}
	}

	if (zmq_close(mq_socket) == -1)
		pfpanic_zmq_close();

	if (zmq_ctx_term(mq_context) == -1)
		pfpanic_zmq_ctx_term();
	
	exit(EX_OK);
}

