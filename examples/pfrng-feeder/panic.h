#pragma once

#ifndef PF_PANIC_H
#define PF_PANIC_H

/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <errno.h>

#define pfpanic_open(B) \
	__pfpanic_open(B, 0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_open_syslog(B,A) \
	__pfpanic_open(B, 1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_read(B) \
	__pfpanic_read(B, 0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_read_syslog(B,A) \
	__pfpanic_read(B, 1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_close(B) \
	__pfpanic_close(B, 0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_close_syslog(B,A) \
	__pfpanic_close(B, 1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_ioctl(B) \
	__pfpanic_ioctl(B, 0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_ioctl_syslog(B,A) \
	__pfpanic_ioctl(B, 1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zero() \
	__pfpanic_zero(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zero_syslog(A) \
	__pfpanic_zero(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_signal() \
	__pfpanic_signal(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_signal_syslog(A) \
	__pfpanic_signal(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_sigmask() \
	__pfpanic_sigmask(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_sigmask_syslog(A) \
	__pfpanic_sigmask(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_pselect() \
	__pfpanic_pselect(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_pselect_syslog(A) \
	__pfpanic_pselect(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_malloc() \
	__pfpanic_malloc(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_malloc_syslog(A) \
	__pfpanic_malloc(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_daemon() \
	__pfpanic_daemon(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_daemon_syslog(A) \
	__pfpanic_daemon(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_pthread_create() \
	__pfpanic_pthread_create(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_pthread_create_syslog(A) \
	__pfpanic_pthread_create(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_pthread_join() \
	__pfpanic_pthread_join(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_pthread_join_syslog(A) \
	__pfpanic_pthread_join(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_context_create() \
	__pfpanic_zmq_context_create(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_context_create_syslog(A) \
	__pfpanic_zmq_context_create(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_socket_create() \
	__pfpanic_zmq_socket_create(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_socket_create_syslog(A) \
	__pfpanic_zmq_socket_create(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_socket_bind() \
	__pfpanic_zmq_socket_bind(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_socket_bind_syslog(A) \
	__pfpanic_zmq_socket_bind(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_socket_connect() \
	__pfpanic_zmq_socket_connect(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_socket_connect_syslog(A) \
	__pfpanic_zmq_socket_connect(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_send() \
	__pfpanic_zmq_send(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_send_syslog(A) \
	__pfpanic_zmq_send(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_recv() \
	__pfpanic_zmq_recv(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_recv_syslog(A) \
	__pfpanic_zmq_recv(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_close() \
	__pfpanic_zmq_close(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_close_syslog(A) \
	__pfpanic_zmq_close(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_zmq_ctx_term() \
	__pfpanic_zmq_ctx_term(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_zmq_ctx_term_syslog(A) \
	__pfpanic_zmq_ctx_term(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_clock_gettime() \
	__pfpanic_clock_gettime(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_clock_gettime_syslog(A) \
	__pfpanic_clock_gettime(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_mutex_lock() \
	__pfpanic_mutex_lock(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_mutex_lock_syslog(A) \
	__pfpanic_mutex_lock(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_mutex_unlock() \
	__pfpanic_mutex_unlock(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_mutex_unlock_syslog(A) \
	__pfpanic_mutex_unlock(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_mutex_init() \
	__pfpanic_mutex_init(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_mutex_init_syslog(A) \
	__pfpanic_mutex_init(1, A, __func__, __FILE__, __LINE__, errno)

#define pfpanic_mutex_destroy() \
	__pfpanic_mutex_destroy(0, 1, __func__, __FILE__, __LINE__, errno)
#define pfpanic_mutex_destroy_syslog(A) \
	__pfpanic_mutex_destroy(1, A, __func__, __FILE__, __LINE__, errno)

#define PF_PANIC_BACKTRACE_SIZE 10

void pfpanic_non_root(void);
void __pfpanic_open(const char* _failed_file, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_read(const char* _failed_file, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_close(const char* _failed_file, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_ioctl(const char* _ioctl, unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zero(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_signal(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_sigmask(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_pselect(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_malloc(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_daemon(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_pthread_create(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_pthread_join(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_context_create(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_socket_create(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_socket_bind(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_socket_connect(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_send(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_recv(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_close(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_zmq_ctx_term(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_clock_gettime(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_mutex_lock(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_mutex_unlock(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_mutex_init(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);
void __pfpanic_mutex_destroy(unsigned short _syslog, unsigned short _verbose, const char* _function_name, const char* _file_name, int _line_number, int _errno);

#endif /* PF_PANIC_H */

