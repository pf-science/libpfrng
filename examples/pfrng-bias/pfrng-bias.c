/*
    libpfrng — CPU jitter based random number generator
    © 2013–2014, Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published by
    the Free Software Foundation, version 3 of the License.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <getopt.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <sysexits.h>

#include "../../libpfrng.h"

#define PASSES		100000
#define ANSAMBLES	100

int main(int argc, char** argv)
{
	int opts;
	unsigned int fast = 0;
	struct option longopts[] =
	{
		{"fast", no_argument, NULL, 'f'},
		{0, 0, 0, 0}
	};
	while ((opts = getopt_long(argc, argv, "f", longopts, NULL)) != -1)
		switch (opts)
		{
			case 'f':
				fast = 1;
				break;
			default:
				fprintf(stderr, "Unknown option: %c\n", opts);
				exit(EX_USAGE);
				break;
		}

	pfrng_init();

	printf("Generating ansambles...\n");
	pfrng_stats_t ansambles[ANSAMBLES];
	volatile double rnd __attribute__ ((unused));
	for (uint64_t i = 0; i < ANSAMBLES; i++)
	{
		printf("\t%ju of %d...\n", i, ANSAMBLES);
		for (uint64_t j = 0; j < PASSES; j++)
		{
			if (fast)
				rnd = pfrng_get_lf_fast();
			else
				rnd = pfrng_get_lf();
		}

		ansambles[i] = pfrng_get_stats();
		pfrng_reset_stats();
	}

	printf("Normalizing ansambles...\n");
	pfrng_stats_t resulted;
	for (uint64_t i = 0; i < UINT64_SIZE; i++)
		resulted.freq[i] = 0;
	for (uint64_t i = 0; i < ANSAMBLES; i++)
	{
		uint64_t min_value = ansambles[i].freq[0];
		for (uint64_t j = 1; j < UINT64_SIZE; j++)
			if (ansambles[i].freq[j] < min_value)
				min_value = ansambles[i].freq[j];
		for (uint64_t j = 0; j < UINT64_SIZE; j++)
		{
			ansambles[i].freq[j] -= min_value;
			resulted.freq[j] += ansambles[i].freq[j];
		}
	}

	printf("Building frequency table...\n");
	FILE* fout = fopen("out.freq", "w");
	for (uint64_t i = 0; i < UINT64_SIZE; i++)
	{
		fprintf(fout, "%ju\t%ju\n", i, resulted.freq[i]);
		fflush(fout);
	}
	fclose(fout);
	
	pfrng_done();

	exit(EX_OK);
}

