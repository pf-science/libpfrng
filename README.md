libpfrng
========

Simple and stupid, but high-quality true RNG, based on CPU timings jitter.

The original idea is stolen from [jitterentropy](http://www.chronox.de/jent/doc/CPU-Jitter-NPTRNG.html) project.

libpfrng is userspace, multithreaded (using OpenMP) and -Ofast-friendly library.
