/**
 * @file libpfrng.c
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 * @version 0.0.9
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * libpfrng — CPU jitter based random number generator, library main file
 */

#include <math.h>
#if defined (_OPENMP)
#include <omp.h>
#endif
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/param.h>
#include <sysexits.h>
#include <time.h>
#include <unistd.h>
#include "libpfrng.h"

/// CPU threads in use
static int cpus_count;

/// Handles useful debug statistics
pfrng_stats_t stats;

/// Handles per-thread mixer values
static uint64_t *mixer_output;

/// Handles fast PRNG seed
static uint64_t fast_seed;

/// Handles fast PRNG calls count
static unsigned long long fast_calls;

/// Handles fast PRNG lock
static pthread_mutex_t fast_lock;

/// Handles per-thread previous timings
static struct timespec *time_prev;

/// Array of masks for bit rolling weighting
static const uint64_t rolling_rounds[][2] =
{
	{(uint64_t)0x1 << 0, 0},
	{(uint64_t)0x3 << 1, 1},
	{(uint64_t)0xF << 3, 2},
	{(uint64_t)0xFF << 7, 4},
	{(uint64_t)0xFFFF << 15, 8},
	{(uint64_t)0xFFFFFFFF << 31, 16}
};

/// Shows whether library is initialized and ready to use
static unsigned short initialized = 0;

/**
 * Returns Hamming weight of given number
 *
 * This function returns the amount of bits set in
 * given integer number. If GCC is used, __builtin_popcount()
 * is invoked instead of own algorithm.
 *
 * @param _value given number to examine
 *
 * @return Hamming weight of given number
 */
static uint8_t pfrng_popcount(uint64_t _value)
{
#ifdef __GNUC__
	return __builtin_popcount(_value);
#else /* __GNUC__ */
	uint8_t ret = 0;
	for (uint8_t i = 0; i < UINT64_SIZE; i++)
		if ((_value >> i) & ((uint64_t)1))
			ret++;
	return ret;
#endif /* __GNUC__ */
}

/**
 * Collapses uint64_t to one bit
 *
 * This function collapses unsigned long integer into one bit
 * using special weighting table.
 *
 * @param _x unsigned long integer to be rolled
 *
 * @return unsigned short integer (8 bits long) containing one meaningful but
 */
static inline uint8_t pfrng_get_rolled_bit(uint64_t _x)
{
	uint8_t sum = 0;

	// As _x is time value in nanoseconds, it's obvious that rightmost bits
	// change faster than leftmost. So it's necessary to sum them with
	// different weights. Weighting is implemented via scaling window.
	// On each step the amount of bits set in current windows is examined.
	// If calculated amount is larger than threshold (half of window size),
	// the resulting sum is XOR'ed. That's why it's required less rightmost bits
	// than leftmost to trigger XOR.
	for (unsigned int i = 0; i < sizeof(rolling_rounds) / sizeof(rolling_rounds[0]); i++)
		if (pfrng_popcount(_x & rolling_rounds[i][0]) > rolling_rounds[i][1])
			sum ^= 1;

	return sum;
}

/**
 * Adds extra bit to mixer variable
 *
 * This function is used for adding extra random bit to mixer variable.
 * Mixer variable is used to shuffle random output in order to prevent
 * bits freqency spikes and to increase entropy greatly. If the bit is 1,
 * mixer variable is being shuffled additionally with pre-defined constant,
 * stolen from jitterentropy.
 *
 * @param _bit unsigned short integer representing input bit
 * @param _index CPU thread number
 */
static inline void pfrng_add_bit_to_mixer(uint8_t _bit, int _index)
{
	mixer_output[_index] <<= 1;
	mixer_output[_index] ^= _bit;

	// Mixing the mixer using some pre-defined constant
	if (_bit)
		mixer_output[_index] ^= 0x98badcfe10325476;
}

/**
 * Returns timings difference
 *
 * This function returns CPU timings difference in order to extract entropy from
 * this.
 *
 * @param _index CPU thread number
 *
 * @return unsigned long integer that contains difference between current time
 * 	and previously stored time in nanoseconds
 */
static inline uint64_t pfrng_get_diff(int _index)
{
	struct timespec time_current;

	// Using CLOCK_REALTIME because
	// a) it's quite fast
	// b) it's subjected to time changes via NTP and adjtime (additional entropy!)
	if (unlikely(clock_gettime(CLOCK_REALTIME, &time_current) != 0))
		exit(EX_OSERR);

	// Return diff in nanoseconds and store the result as previous timings
	uint64_t ret =
		(time_current.tv_sec * 1000000000L + time_current.tv_nsec) -
		(time_prev[_index].tv_sec * 1000000000L + time_prev[_index].tv_nsec);
	time_prev[_index] = time_current;

	return ret;
}

/**
 * Rolls timings difference into biased bit
 *
 * This function returns unsigned short integer that contains one bit only.
 * This bit represents collapsed timings difference and is potentially biased.
 *
 * @param _index CPU thread number
 *
 * @return unsigned short integer that contains one bit
 */
static inline uint8_t pfrng_get_biased_bit(int _index)
{
	return pfrng_get_rolled_bit(pfrng_get_diff(_index));
}

/**
 * Returns unbiased random bit
 *
 * This function filters out biased bits using Von Neumann
 * algorithm and returns unbiased random bit.
 *
 * @param _index CPU thread number
 *
 * @return unsigned short integer that contains unbiased random bit
 */
static inline uint8_t pfrng_get_unbiased_bit(int _index)
{
	while (1)
	{
		// Here we just say hello to Von Neumann
		uint8_t a = pfrng_get_biased_bit(_index);
		uint8_t b = pfrng_get_biased_bit(_index);
		if (a == b)
		{
#ifdef CONFIG_STATS
			if (unlikely(pthread_mutex_lock(&stats.bits_misses_lock) != 0))
				exit(EX_OSERR);
			stats.bits_misses++;
			if (unlikely(pthread_mutex_unlock(&stats.bits_misses_lock) != 0))
				exit(EX_OSERR);
#endif
			continue;
		} else
		{
			// Why wasting bit? Add it
			pfrng_add_bit_to_mixer(b, _index);
			return a;
		}
	}
}

/**
 * Returns unsigned long random integer
 *
 * This function returns unsigned long integer that consists of
 * random bits.
 *
 * @return unsigned long random integer
 */
uint64_t __pfrng_get_u64_slow(void)
{
	uint64_t sum = 0;

	// Here we get 64 bits step-by-step and add them to result chunk.
	// Result is being filled in parallel, internal values do not intersect
	// as they are used on per-thread basis
#if defined (_OPENMP)
#pragma omp parallel for shared(sum)
#endif
	for (uint8_t i = 0; i < UINT64_SIZE; i++)
	{
#if defined (_OPENMP)
		int index = omp_get_thread_num();
#else
		int index = 0;
#endif
		if (pfrng_get_unbiased_bit(index))
		{
			sum |= ((uint64_t)1) << i;
#if defined (_OPENMP)
#pragma omp critical (update_sum)
#endif
			{
				// Mixing sum with another pre-defined constant
				sum ^= 0x67452301efcdab89;
				// And with own mixer
				sum ^= mixer_output[index];
			}
		}
	}

	return sum;
}

/**
 * Returns fast pseudo-random number
 *
 * This function returns random number generated by fast XOR-shifting
 * algorithm with periodical rehashing using true RNG.
 *
 * @return unsigned long pseudo-random integer
 */
uint64_t __pfrng_get_u64_fast(void)
{
	uint64_t ret;

	if (unlikely(pthread_mutex_lock(&fast_lock) != 0))
		exit(EX_OSERR);

	if (++fast_calls > PFRNG_FAST_REHASHING_THRESHOLD)
	{
		fast_seed ^= __pfrng_get_u64_slow();
		fast_calls = 0;
	}

	fast_seed ^= (fast_seed << 21);
	fast_seed ^= (fast_seed >> 35);
	fast_seed ^= (fast_seed << 4);

	ret = fast_seed;

	if (unlikely(pthread_mutex_unlock(&fast_lock) != 0))
		exit(EX_OSERR);

	return ret;
}

/**
 * Switches between fast and slow RNG
 *
 * This function returns true or pseudo-random integer
 * based on argument.
 *
 * @param _fast indicates using fast PRNG
 *
 * @return unsigned long (pseudo)random integer
 */
inline uint64_t __pfrng_get_u64(unsigned int _fast)
{
	if (initialized == 0)
		exit(EX_SOFTWARE);

	uint64_t ret;

	if (_fast)
		ret = __pfrng_get_u64_fast();
	else
		ret = __pfrng_get_u64_slow();

#ifdef CONFIG_STATS
	if (unlikely(pthread_mutex_lock(&stats.freq_lock) != 0))
		exit(EX_OSERR);
	for (uint8_t i = 0; i < UINT64_SIZE; i++)
		if (!!(ret & (((uint64_t)1) << i)))
			stats.freq[i]++;
	if (unlikely(pthread_mutex_unlock(&stats.freq_lock) != 0))
		exit(EX_OSERR);
#endif

	return ret;
}

/**
 * Returns long float random number from 0…1 range
 *
 * This function returns long float random number that is in fact
 * unsigned long integer normalized to UINT64_MAX value
 *
 * @param _fast indicates using fast PRNG
 *
 * @return long float random number with uniform distribution
 */
double __pfrng_get_lf(unsigned int _fast)
{
	if (initialized == 0)
		exit(EX_SOFTWARE);

	// Simple normalization to have 0..1 value
	return (double)__pfrng_get_u64(_fast) / UINT64_MAX;
}

/**
 * Returns random number with Gaussian distribution
 *
 * This function uses Box-Muller transform to get random number
 * with Gaussian distribution from uniformly distributed long
 * float random number.
 *
 * @param _fast indicates using fast PRNG
 *
 * @return long float random number with Gaussian distribution
 */
double __pfrng_get_gauss(unsigned int _fast)
{
	if (initialized == 0)
		exit(EX_SOFTWARE);

	// Simple Box-Muller transform. Stolen from here:
	// http://www.cs.princeton.edu/courses/archive/spr10/cos226/StdGaussian.java.html
	double r, x, y;
	do
	{
		x = 2.0 * __pfrng_get_lf(_fast) - 1.0;
		y = 2.0 * __pfrng_get_lf(_fast) - 1.0;
		r = pow(x, 2) + pow(y, 2);
	} while (r > 1 || r == 0);
	return x * sqrt(-2.0 * log(r) / r);
}

/**
 * Fills unsigned char buffer variable with random data
 *
 * This function fills unsigned char buffer of given size with random data.
 *
 * @param _buffer pointer to unsigned char buffer
 * @param _buffer_size size of buffer to be filled
 * @param _fast indicates using fast PRNG
 *
 * @return amount of data injected to buffer, in bytes
 */
uint64_t __pfrng_fill_buffer_u(unsigned char *_buffer, uint64_t _buffer_size, unsigned int _fast)
{
	if (initialized == 0)
		exit(EX_SOFTWARE);

	uint64_t filled = 0;

	// Fill by SIZEOF_UINT64 chunks
	while (_buffer_size >= SIZEOF_UINT64)
	{
		uint64_t chunk = __pfrng_get_u64(_fast);
		memcpy(_buffer, &chunk, SIZEOF_UINT64);
		filled += SIZEOF_UINT64;
		_buffer_size -= SIZEOF_UINT64;

		_buffer += SIZEOF_UINT64;
	}

	// Fill the rest (less that SIZEOF_UINT64 if any)
	if (_buffer_size > 0)
	{
		uint64_t chunk = __pfrng_get_u64(_fast);
		memcpy(_buffer, &chunk, _buffer_size);
		filled += _buffer_size;
	}

	return filled;
}

/**
 * Returns various library statistics
 *
 * This function returns statistics structure that contains
 * various debug infrmation about misses and bits frequencies.
 * In order for this function to be useful, compile the library
 * with CONFIG_STATS option.
 *
 * @return statistics structure
 */
pfrng_stats_t pfrng_get_stats(void)
{
	if (initialized == 0)
		exit(EX_SOFTWARE);

	pfrng_stats_t ret;

	if (unlikely(pthread_mutex_lock(&stats.bits_misses_lock) != 0))
		exit(EX_OSERR);
	if (unlikely(pthread_mutex_lock(&stats.freq_lock) != 0))
		exit(EX_OSERR);

	ret = stats;

	if (unlikely(pthread_mutex_unlock(&stats.freq_lock) != 0))
		exit(EX_OSERR);
	if (unlikely(pthread_mutex_unlock(&stats.bits_misses_lock) != 0))
		exit(EX_OSERR);

	return ret;
}

/**
 * Reset library statistics
 *
 * This function resets statistics structure that contains
 * various debug infrmation about misses and bits frequencies.
 * In order for this function to be useful, compile the library
 * with CONFIG_STATS option.
 */
void pfrng_reset_stats(void)
{
	if (initialized == 0)
		exit(EX_SOFTWARE);

	if (unlikely(pthread_mutex_lock(&stats.bits_misses_lock) != 0))
		exit(EX_OSERR);
	if (unlikely(pthread_mutex_lock(&stats.freq_lock) != 0))
		exit(EX_OSERR);

	stats.bits_misses = 0;
	for (uint8_t i = 0; i < UINT64_SIZE; i++)
		stats.freq[i] = 0;

	if (unlikely(pthread_mutex_unlock(&stats.freq_lock) != 0))
		exit(EX_OSERR);
	if (unlikely(pthread_mutex_unlock(&stats.bits_misses_lock) != 0))
		exit(EX_OSERR);
}

/**
 * Library constructor
 *
 * This function sets up initial values of internal variables
 * and fills mixer variable with initial random data. Keep in mind
 * that in case of any failure the constructor calls exit().
 */
void pfrng_init(void)
{
	// Init mutexes
	if (unlikely(pthread_mutex_init(&stats.bits_misses_lock, NULL) != 0 ||
			pthread_mutex_init(&stats.freq_lock, NULL) != 0 ||
			pthread_mutex_init(&fast_lock, NULL) != 0))
		exit(EX_OSERR);

	// Get CPUs count
#if defined (_OPENMP)
	cpus_count = omp_get_max_threads();
#else
	cpus_count = 1;
#endif

	// Allocate memory for internal variables
	mixer_output = malloc(cpus_count * SIZEOF_UINT64);
	time_prev = malloc(cpus_count * sizeof(struct timespec));
	if (unlikely(mixer_output == NULL || time_prev == NULL))
		exit(EX_OSERR);

	// Init statistic variables
	stats.bits_misses = 0;
	for (uint8_t i = 0; i < UINT64_SIZE; i++)
		stats.freq[i] = 0;
	fast_calls = 0;

	// Get initial timing values
	for (int i = 0; i < cpus_count; i++)
	{
		while (1)
		{
			if (unlikely(clock_gettime(CLOCK_REALTIME, &time_prev[i]) != 0))
				exit(EX_OSERR);
			if (likely(time_prev[i].tv_sec != 0))
				break;
		}
	}

	// Get initial random mixing values
	for (int i = 0; i < cpus_count; i++)
		mixer_output[i] = __pfrng_get_u64_slow();
	fast_seed = __pfrng_get_u64_slow();

	initialized = 1;
}

/**
 * Library destructor
 *
 * This function acts on library unloading and cleans up
 * all internal variables.
 */
void pfrng_done(void)
{
	// Cleanup
	initialized = 0;

	stats.bits_misses = 0;
	for (uint8_t i = 0; i < UINT64_SIZE; i++)
		stats.freq[i] = 0;
	fast_calls = 0;

	for (int i = 0; i < cpus_count; i++)
	{
		mixer_output[i] = 0;
		time_prev[i].tv_sec = 0;
		time_prev[i].tv_nsec = 0;
	}
	fast_seed = 0;

	// Free memory
	free(mixer_output);
	free(time_prev);

	// Destroy mutexes
	if (unlikely(pthread_mutex_destroy(&stats.bits_misses_lock) != 0 ||
			pthread_mutex_destroy(&stats.freq_lock) != 0 ||
			pthread_mutex_destroy(&fast_lock) != 0))
		exit(EX_OSERR);
}

