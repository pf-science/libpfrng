/**
 * @file libpfrng.h
 * @author Oleksandr Natalenko aka post-factum <oleksandr@natalenko.name>
 * @version 0.0.9
 *
 * @section LICENSE
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, version 3 of the License.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 *
 * @section DESCRIPTION
 *
 * libpfrng — CPU jitter based random number generator, library header file
 */

#include <limits.h>
#include <stdint.h>

/// Major library version
#define LIBPFRNG_MAJOR		0

/// Minor library version
#define LIBPFRNG_MINOR		0

/// Library release version
#define LIBPFRNG_RELEASE	9

/// Library API version
#define LIBPFRNG_API		6

/// Fast PRNG rehashing threshold
#define PFRNG_FAST_REHASHING_THRESHOLD	16384

/// uint64_t size constant
#define SIZEOF_UINT64	sizeof(uint64_t)

/// uint64_t bits count constant
#define UINT64_SIZE	(CHAR_BIT * SIZEOF_UINT64)

#ifdef __GNUC__
/// gcc userspace analogue to Linux likely(x) macro
#define likely(x)       __builtin_expect(!!(x), 1)

/// gcc userspace analogue to Linux likely(x) macro
#define unlikely(x)     __builtin_expect(!!(x), 0)
#else /* __GNUC__ */

/// likely macro does nothing if compiler is not gcc
#define likely(x)       (x)

/// unlikely macro does nothing if compiler is not gcc
#define unlikely(x)     (x)
#endif /* __GNUC__ */

/**
 * Statistics structure typedef
 *
 * This struct contains various useful debug information
 * such as misses counters and frequency counters
 */
typedef struct stats
{
	uint64_t
		/// Von Neumann unbiasing bits misses counter
		bits_misses,

		/// bit frequencies counter
		freq[UINT64_SIZE];

	pthread_mutex_t
		/// Von Neumann unbiasing bits misses counter lock
		bits_misses_lock,

		/// bit frequencies counter lock
		freq_lock;
} pfrng_stats_t;

#define pfrng_get_u64() \
	__pfrng_get_u64(0)
#define pfrng_get_u64_slow() \
	__pfrng_get_u64(0)
#define pfrng_get_u64_fast() \
	__pfrng_get_u64(1)
#define pfrng_get_lf() \
	__pfrng_get_lf(0)
#define pfrng_get_lf_fast() \
	__pfrng_get_lf(1)
#define pfrng_get_gauss() \
	__pfrng_get_gauss(0)
#define pfrng_get_gauss_fast() \
	__pfrng_get_gauss(1)
#define pfrng_fill_buffer_u(A, B) \
	__pfrng_fill_buffer_u(A, B, 0)
#define pfrng_fill_buffer_u_fast(A, B) \
	__pfrng_fill_buffer_u(A, B, 1)

void pfrng_init();
void pfrng_done();
uint64_t __pfrng_get_u64(unsigned int _fast);
double __pfrng_get_lf(unsigned int _fast);
double __pfrng_get_gauss(unsigned int _fast);
uint64_t __pfrng_fill_buffer_u(unsigned char *_buffer, uint64_t _buffer_size, unsigned int _fast);
pfrng_stats_t pfrng_get_stats(void);
void pfrng_reset_stats(void);

